export const configuration = {
  ...window.ENV,
  BASENAME: process.env.PUBLIC_URL,
  NODE_ENV: process.env.NODE_ENV,
  API_AUTH_URL: process.env.API_AUTH_URL,
};
