import { observable, action, computed, autorun } from "mobx";
import { observer } from "mobx-react-lite";
import { Navigate } from "react-router-dom";
import * as io from "io-ts";
import { SyncTrunk } from "mobx-sync";
import jwt_decode from "jwt-decode";
import { notifications, NotificationType } from "./notifications";
import { createContext, useContext, FC } from "react";
import React from "react";
import { fetch } from "./fetch";
import {
  createQueryStateObservable,
  Maybe,
  QueryError,
} from "./createQueryStateObservable";
import { configuration } from "configuration";

const API_AUTH_URL = configuration.AUTH_API_URL;

export interface JWT {
  id: number;
  iss: string;
  admin: boolean;
  verified?: Maybe<boolean>;
  exp: number;
}

export const auth = observable(
  {
    signInState: createQueryStateObservable({
      dataCodec: io.interface({ token: io.string }),
      bodyCodec: io.interface({
        email: io.string,
        password: io.string,
      }),
      fetch: ({ body }) =>
        fetch(`${API_AUTH_URL}/login`, { method: "POST", body }),
    }),
    signIn(credentials: { email: string; password: string }) {
      return this.signInState.fetch({ body: credentials });
    },
    signUpState: createQueryStateObservable({
      bodyCodec: io.interface({
        email: io.string,
        password: io.string,
      }),
      fetch: ({ body }) =>
        fetch(`${API_AUTH_URL}/users`, { method: "POST", body }),
    }),
    signUp(credentials: { email: string; password: string }) {
      return this.signUpState.fetch({ body: credentials });
    },
    invitationState: createQueryStateObservable({
      paramsCodec: io.interface({ code: io.string }),
      dataCodec: io.interface({
        already_accepted: io.boolean,
        already_expired: io.boolean,
        need_password: io.boolean,
      }),
      fetch: ({ params: { code } }) =>
        fetch(`${API_AUTH_URL}/invitations/${code}`),
    }),
    fetchInvitationState(code: string) {
      return this.invitationState.fetch({
        params: { code },
      });
    },
    accountConfirmationState: createQueryStateObservable({
      paramsCodec: io.interface({ confirmationToken: io.string }),
      bodyCodec: io.partial({ password: io.string }),
      fetch: ({ params: { confirmationToken }, body }) =>
        fetch(`${API_AUTH_URL}/invitation/${confirmationToken}`, {
          method: "PUT",
          body,
        }),
    }),
    accountConfirmation(confirmationToken: string, password?: string) {
      return this.accountConfirmationState.fetch({
        params: { confirmationToken },
        body: password ? { password } : {},
      });
    },
    signOut() {
      this.signInState.data = null;
    },
    passwordResetState: createQueryStateObservable({
      bodyCodec: io.interface({ password: io.string }),
      paramsCodec: io.interface({ resetToken: io.string }),
      dataCodec: io.unknown,
      fetch: ({ params: { resetToken }, body }) =>
        fetch(`${API_AUTH_URL}/forgotten-password/${resetToken}`, {
          method: "PUT",
          body,
        }),
    }),
    passwordReset(resetToken: string, password: string) {
      return this.passwordResetState.fetch({
        params: { resetToken },
        body: { password },
      });
    },
    passwordResetRequestState: createQueryStateObservable({
      bodyCodec: io.interface({
        email: io.string,
      }),
      fetch: ({ body }) =>
        fetch(`${API_AUTH_URL}/forgotten-password`, {
          method: "POST",
          body,
        }),
    }),
    passwordResetRequest(data: { email: string }) {
      return this.passwordResetRequestState.fetch({
        body: {
          ...data,
          route_link: "/auth/password-reset/",
        },
      });
    },
    get user() {
      const data = this.signInState.data;
      if (!data) {
        return null;
      }
      try {
        const userData = jwt_decode(data.token) as JWT;
        if (new Date(parseInt(`${userData.exp}000`, 10)) < new Date()) {
          return null;
        }
        return userData;
      } catch (error) {
        return null;
      }
    },
    urlAfterLogin: null as string | null,
    setUrlAfterLogin(url: string | null) {
      this.urlAfterLogin = url;
    },
  },
  {
    signIn: action.bound,
    accountConfirmation: action.bound,
    signOut: action.bound,
    passwordReset: action.bound,
    passwordResetRequest: action.bound,
    user: computed,
    setUrlAfterLogin: action.bound,
  }
);

export const AuthStateContext = createContext<{ auth: typeof auth }>({ auth });

export const useAuthStateContext = () => useContext(AuthStateContext);

export const withAuthenticatedUser =
  (
    { authRedirectTo }: { authRedirectTo: string } = { authRedirectTo: "/auth" }
  ) =>
  (Component: FC) =>
    observer(() => {
      const { auth: authState } = useAuthStateContext();
      if (!authState.user) {
        return React.createElement(Navigate, { to: authRedirectTo });
      }

      return React.createElement(Component);
    });

const trunk = new SyncTrunk(auth.signInState, {
  storageKey: "state/modules/auth:signInState",
  storage: localStorage,
});
trunk.init();

const addErrorNotification = (error: QueryError) => {
  const errorData = error.data;
  const errorTitle =
    typeof errorData === "object" && errorData !== null
      ? errorData.error_message
      : error.message;
  notifications.addNotification({
    title: errorTitle,
    type: NotificationType.error,
  });
};

autorun(() => {
  if (auth.signInState.error) {
    addErrorNotification(auth.signInState.error);
  }
});

autorun(() => {
  if (auth.passwordResetRequestState.error) {
    addErrorNotification(auth.passwordResetRequestState.error);
  }
});

autorun(() => {
  if (auth.signUpState.error) {
    addErrorNotification(auth.signUpState.error);
  }
});
