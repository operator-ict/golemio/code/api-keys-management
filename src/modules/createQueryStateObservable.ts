import { observable, transaction, action } from "mobx";
import * as io from "io-ts";
import { isRight } from "fp-ts/lib/Either";
import reporter from "io-ts-reporters";
import { stringify, parse } from "query-string";
import { configuration } from "configuration";

const isDev = configuration.NODE_ENV !== "production";
export type Maybe<A> = A | null;

const devLogError = (...args: Parameters<typeof console.error>) => {
  if (isDev) {
    console.error(...args);
  }
};

const createQueryParamsToStringCodec = <QueryParams extends any>(type: io.Type<QueryParams>) =>
  new io.Type<QueryParams, string, string>(
    "QueryParamsToString",
    type.is,
    // validate
    (input, context) => {
      const res = type.decode(parse(input));
      if (isRight(res)) {
        return io.success(res.right);
      } else {
        return io.failure(input, context);
      }
    },
    // encode
    (x) => {
      const result = stringify(x);
      if (result) {
        return `?${result}`;
      }
      return "";
    }
  );

const isPlainTextResponse = (response: Response) => {
  return response.headers.get("content-type").includes("text");
};

const getData = async (response: Response, shouldGetBlob: boolean) => {
  if (shouldGetBlob) {
    return await response.blob();
  }

  const isPlainText = isPlainTextResponse(response);
  const data = isPlainText ? await response.text() : await response.json();
  return data;
};

const createBodyToStringCodec = <Body extends any>(type: io.Type<Body>) =>
  new io.Type<Body, string, string>(
    "BodyToString",
    type.is,
    // validate
    (input, context) => {
      const res = type.decode(JSON.parse(input));
      if (isRight(res)) {
        return io.success(res.right);
      } else {
        return io.failure(input, context);
      }
    },
    // encode
    (x) => {
      return JSON.stringify(x);
    }
  );

interface ErrorData {
  error_message: string;
  error_class_name?: string;
  error_status?: number;
  error_info?: string;
  stack_trace?: string;
}

export class QueryError extends Error {
  public data: ErrorData;
  constructor(message: string, data: ErrorData) {
    super(message);
    this.data = data;
  }
}

const hasQueryParams = <P extends any>(options: unknown): options is P => {
  return typeof options === "object" && !!options && "queryParams" in options;
};

const hasBody = <P extends { body: any }>(options: unknown): options is P => {
  return typeof options === "object" && !!options && "body" in options;
};

const hasFormDataBody = (options: unknown): options is { body: FormData } => {
  return hasBody(options) && options.body instanceof FormData;
};

const hasParams = <P extends any>(options: unknown): options is P => {
  return typeof options === "object" && !!options && "params" in options;
};

export interface CreateQueryStateOptions<ApiData, AppData, Params, Body, QueryParams> {
  dataCodec?: io.Type<ApiData>;
  paramsCodec?: io.Type<Params>;
  shouldGetBlob?: boolean;
  bodyCodec?: io.Type<Body>;
  queryParamsCodec?: io.Type<QueryParams>;
  transform?: (data: ApiData) => AppData;
  fetch: (requestSettings: {
    queryString: string;
    body: BodyInit;
    params?: Params;
  }) => ReturnType<typeof fetch>;
}
export interface QueryState<AppData, Params, Body, QueryParams> {
  isLoading: boolean;
  queryParams: Maybe<QueryParams>;
  data: Maybe<AppData>;
  error: Maybe<QueryError>;
  fetch: (
    fetchParams: (QueryParams extends undefined ? {} : { queryParams: QueryParams }) &
      (Body extends undefined ? {} : { body: Body }) &
      (Params extends undefined ? {} : { params: Params })
  ) => Promise<AppData | null>;
  receive: (data: AppData | null) => void;
  receiveError: (error: QueryError) => void;
}
export function createQueryStateObservable<
  ApiData,
  AppData = ApiData,
  Params = undefined,
  Body = undefined,
  QueryParams = undefined
>(options: CreateQueryStateOptions<ApiData, AppData, Params, Body, QueryParams>) {
  const { fetch, dataCodec, queryParamsCodec, bodyCodec, paramsCodec, transform, shouldGetBlob } =
    options;

  const queryParamsToStringCodec = queryParamsCodec
    ? createQueryParamsToStringCodec(queryParamsCodec)
    : null;

  const bodyToStringCodec = bodyCodec ? createBodyToStringCodec(bodyCodec) : null;

  return observable<QueryState<AppData, Params, Body, QueryParams>>(
    {
      isLoading: false,
      data: null,
      queryParams: null,
      error: null,
      async fetch(fetchParams) {
        let queryString = "";
        if (hasQueryParams<{ queryParams: QueryParams }>(fetchParams) && queryParamsToStringCodec) {
          this.queryParams = fetchParams.queryParams;
          queryString = queryParamsToStringCodec.encode(this.queryParams);
        } else {
          this.queryParams = null;
        }

        let body: BodyInit = "";
        if (hasFormDataBody(fetchParams)) {
          body = fetchParams.body;
        } else if (hasBody<{ body: Body }>(fetchParams) && bodyToStringCodec) {
          body = bodyToStringCodec.encode(fetchParams.body);
        }

        let params: Params | undefined;
        if (hasParams<{ params: Params }>(fetchParams) && paramsCodec) {
          params = paramsCodec.encode(fetchParams.params);
        }

        this.isLoading = true;
        try {
          const response = await fetch({ queryString, body, params });
          if (response.ok) {
            if (!dataCodec) {
              this.receive(null);
              return true;
            }

            const responseData = await getData(response, shouldGetBlob || false);

            if (shouldGetBlob) {
              this.receive(responseData);
              return responseData;
            }

            const dataOrError = dataCodec.decode(responseData);

            if (isRight(dataOrError)) {
              const data = dataOrError.right;
              const finalData = transform ? transform(data) : (data as unknown as AppData);
              this.receive(finalData);
              return finalData;
            } else {
              devLogError("Incorrect data format:", reporter.report(dataOrError));
              this.receiveError(new QueryError("Invalid response data", null));
            }
          } else {
            const responseData = await getData(response, false);
            devLogError("Error response:", responseData);
            if (isPlainTextResponse(response)) {
              this.receiveError(new QueryError(responseData, null));
            } else {
              this.receiveError(new QueryError("Error response", responseData));
            }
          }
        } catch (error) {
          devLogError("Fetch error:", error);
          this.receiveError(new QueryError(error.message, null));
        }
        return null;
      },
      receive(data) {
        transaction(() => {
          this.isLoading = false;
          this.error = null;
          this.data = data;
        });
      },
      receiveError(error) {
        transaction(() => {
          this.isLoading = false;
          this.error = error;
          this.data = null;
        });
      },
    },
    {
      fetch: action.bound,
      receive: action.bound,
      receiveError: action.bound,
      data: observable.ref,
    }
  );
}
