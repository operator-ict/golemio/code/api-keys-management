import isomorphicFetch from "isomorphic-fetch";
import { observable } from "mobx";
import { SyncTrunk } from "mobx-sync";

const authData = observable({ data: { token: null } });
const trunk = new SyncTrunk(authData, {
  storageKey: "state/modules/auth:signInState",
  storage: localStorage,
});
trunk.init();

/**
 * This is an application fetch function
 */
export const fetch = async (url: string, options: RequestInit = {}) => {
  trunk.init();
  const token = authData.data && authData.data.token;
  return isomorphicFetch(url, {
    ...options,
    headers: {
      ...(options.body && typeof options.body === "string"
        ? { "Content-Type": "application/json" }
        : {}),
      Accept: "application/json",
      ...(token ? { "x-access-token": token } : {}),
      ...(options.headers || {}),
    },
  });
};
