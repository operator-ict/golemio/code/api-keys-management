import * as io from "io-ts";
// import { extendObservable, computed } from 'mobx';
import { ApiKeysResponseIO } from "codecs/ApiKeysResponseIO";
import { createQueryStateObservable } from "modules/createQueryStateObservable";
import { fetch } from "modules/fetch";
import { configuration } from "configuration";

const apiKeysQueryStateObservable = createQueryStateObservable({
  dataCodec: ApiKeysResponseIO,
  transform: (a) => a,
  fetch: () => fetch(`${configuration.AUTH_API_URL}/api-keys`),
});

// export const apiKeys = extendObservable(apiKeysQueryStateObservable, {}, {});
export const apiKeys = apiKeysQueryStateObservable;

export const deleteApiKeyQueryStateObservableFactory = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ id: io.union([io.string, io.number]) }),
    fetch: ({ params }) => {
      if (params) {
        const { id } = params;
        return fetch(`${configuration.AUTH_API_URL}/api-key/${id}`, {
          method: "delete",
        });
      }
      throw new Error();
    },
  });

export const createApiKeyQueryStateObservable = createQueryStateObservable({
  dataCodec: io.any,
  fetch: () => {
    return fetch(`${configuration.AUTH_API_URL}/api-key`, {
      method: "POST",
    });
  },
});
