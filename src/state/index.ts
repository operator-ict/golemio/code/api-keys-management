import { observable } from "mobx";
import { auth } from "modules/auth";
import { notifications } from "modules/notifications";
import { settings } from "modules/settings";
import { apiKeys } from "state/modules/apiKeys";

export const appState = observable({
  auth,
  apiKeys,
  notifications,
  settings,
});
