import { FC } from "react";
import { ROUTE_TAC, ROUTE_DASHBOARD, ROUTE_AUTH } from "routes";
import { Routes, Route, Navigate, BrowserRouter } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/styles";
import { Lang } from "components/Localization";
import { SnackContainer } from "components/SnackContainer";
import { theme } from "styles/theme";
import { HomePage } from "pages/HomePage";
import { TermsAndConditionsPage } from "pages/TermsAndConditionsPage";
import { appState } from "state";
import { NotificationsStateProvider } from "components/NotificationsStateProvider";
import { AuthStateContext } from "modules/auth";
import { AuthRoutes } from "pages/Auth";
import { configuration } from "configuration";
import AnalyticsTracker from "components/AnalyticsTracker";

export const App: FC = () => {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Lang>
                <NotificationsStateProvider state={appState.notifications}>
                    <SnackContainer />
                    <AuthStateContext.Provider value={appState}>
                        <BrowserRouter basename={configuration.BASENAME}>
                            <AnalyticsTracker />
                            <Routes>
                                <Route
                                    path={`${ROUTE_AUTH.template()}/*`}
                                    element={<AuthRoutes enableSignUp={true} tacTo={ROUTE_TAC.create({})} />}
                                />
                                <Route path={ROUTE_DASHBOARD.template()} element={<HomePage />} />
                                <Route path={ROUTE_TAC.template()} element={<TermsAndConditionsPage />} />
                                <Route path="*" element={<Navigate to={ROUTE_DASHBOARD.template()} />} />
                            </Routes>
                        </BrowserRouter>
                    </AuthStateContext.Provider>
                </NotificationsStateProvider>
            </Lang>
        </ThemeProvider>
    );
};
