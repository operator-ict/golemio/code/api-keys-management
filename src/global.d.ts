declare interface Window {
    ENV: {
        AUTH_API_URL: string;
        DEFAULT_LANG: "cs" | "en";
        DATA_DOMAIN: string;
    };
}
