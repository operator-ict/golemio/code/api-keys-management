import { route } from 'typesafe-react-router';

export const ROUTE_DASHBOARD = route('dashboard');
export const ROUTE_TAC = route('terms-and-conditions');
export const ROUTE_AUTH = route('auth');
