import * as io from 'io-ts';
import { ApiKeyIO } from 'codecs/ApiKeyIO';

export const ApiKeysResponseIO = io.array(ApiKeyIO);
