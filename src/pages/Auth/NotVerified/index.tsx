import { WidgetContent } from "components/WidgetContent";
import React from "react";
import { useTranslation } from "react-i18next";
import SimpleContainer from "../components/SimpleContainer";

const PagePasswordReset = () => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <SimpleContainer>
        <WidgetContent style={{ textAlign: "center", padding: "4rem" }}>
          <h2>{t("page::notVerified::title")}</h2>
          <p>{t("page::notVerified::asterix")}</p>
          {/* <Button>Resend confirmation</Button> */}
        </WidgetContent>
      </SimpleContainer>
    </React.Fragment>
  );
};

export default PagePasswordReset;
