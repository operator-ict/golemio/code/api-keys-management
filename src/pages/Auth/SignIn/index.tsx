import { Button } from "components/Button";
import { FInput } from "components/FInput";
import { FormInputGroup } from "components/FormInputGroup";
import { WidgetContent } from "components/WidgetContent";
import { Formik } from "formik";
import { useAuthStateContext } from "modules/auth";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import * as validation from "validation";
import * as Yup from "yup";
import SimpleContainer from "../components/SimpleContainer";
import cls from "./buildings-signin.module.scss";

export const SignIn = () => {
    const [hasSubmitted, setSubmit] = React.useState(false);
    const { t } = useTranslation();
    const navigate = useNavigate();
    const appState = useAuthStateContext();
    const auth = appState.auth;
    const signingIn = auth.signInState.isLoading;

    const handleLogin = (data: { email: string; password: string }) =>
        auth.signIn({ email: data.email, password: data.password }).then((data: unknown) => {
            if (data) {
                navigate("/dashboard");
            }
        });

    return (
        <React.Fragment>
            <SimpleContainer>
                <WidgetContent>
                    <h2>{t("page::signin::title")}</h2>
                    <br />
                    <Formik
                        initialValues={{
                            confirmation_password: "",
                            email: "",
                            password: "",
                        }}
                        onSubmit={handleLogin}
                        validateOnBlur={hasSubmitted}
                        validateOnChange={false}
                        validationSchema={Yup.object().shape({
                            email: validation.email,
                            password: validation.password,
                            confirmation_password: validation.hfield,
                        })}
                    >
                        {({ handleSubmit }) => (
                            <form
                                onSubmit={(e) => {
                                    setSubmit(true);
                                    handleSubmit(e);
                                }}
                            >
                                <FormInputGroup>
                                    <FInput label={t("email")} name="email" placeholder={t("form::emailPlaceholder")} />
                                    <FInput label={t("password")} name="password" type="password" />
                                    <FInput className={cls.hfield} name="confirmation_password" />
                                    <Button isLoading={signingIn} primary type="submit">
                                        {t("signIn")}
                                    </Button>
                                </FormInputGroup>
                            </form>
                        )}
                    </Formik>
                </WidgetContent>
            </SimpleContainer>
            <p>
                <Trans i18nKey="page::signin::footer">
                    {"Don't have an account yet?"}
                    <Link to="/auth/sign-up">Sign in!</Link>
                    {" Did you "}
                    <Link to="/auth/password-reset">forget a password?</Link>
                </Trans>
            </p>
        </React.Fragment>
    );
};
