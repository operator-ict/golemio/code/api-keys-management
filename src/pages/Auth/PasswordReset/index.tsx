import * as React from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Formik } from "formik";
import { Trans, useTranslation } from "react-i18next";
import * as validation from "validation";
import * as Yup from "yup";

import SimpleContainer from "../components/SimpleContainer";
import { observer } from "mobx-react-lite";
import { useAuthStateContext } from "modules/auth";
import { WidgetContent } from "components/WidgetContent";
import { FormInputGroup } from "components/FormInputGroup";
import { FInput } from "components/FInput";
import { Button } from "components/Button";

export const PagePasswordReset = observer(() => {
  const { t } = useTranslation();
  const appState = useAuthStateContext();
  const [hasReseted, setHasReseted] = React.useState(false);
  const [hasSubmitted, setSubmit] = React.useState(false);
  const navigate = useNavigate();
  const { resetToken } = useParams();

  const isLoading = appState.auth.passwordResetState.isLoading;

  const handleClickReset = (data: { password: string }) => {
    appState.auth.passwordReset(resetToken, data.password).then((res: unknown) => {
      setHasReseted(true);

      setTimeout(() => {
        navigate("/auth/sign-in");
      }, 4000);

      return res;
    });
  };

  return (
    <React.Fragment>
      <SimpleContainer>
        {hasReseted ? (
          <WidgetContent style={{ textAlign: "center", padding: "4rem" }}>
            <h2>{t("page::passwordReset::titleSuccess")}</h2>
            <p>{t("page::passwordReset::asterixSuccess")}</p>
          </WidgetContent>
        ) : (
          <WidgetContent>
            <h2>{t("page::passwordReset::title")}</h2>
            <br />
            <Formik
              initialValues={{ password: "" }}
              onSubmit={handleClickReset}
              validateOnBlur={hasSubmitted}
              validateOnChange={false}
              validationSchema={Yup.object().shape({
                password: validation.password,
              })}
            >
              {({ handleSubmit }) => (
                <form
                  onSubmit={(e) => {
                    setSubmit(true);
                    handleSubmit(e);
                  }}
                >
                  <FormInputGroup>
                    <FInput label={t("newPassword")} name="password" type="password" />
                    <Button isLoading={isLoading} primary type="submit">
                      {t("page::passwordRequest::resetButton")}
                    </Button>
                  </FormInputGroup>
                </form>
              )}
            </Formik>
          </WidgetContent>
        )}
      </SimpleContainer>

      <p>
        <Trans i18nKey="auth::wantToLogIn">
          {"Want to log in? "}
          <Link to="/auth/sign-in">Go to login page.</Link>
        </Trans>
      </p>
    </React.Fragment>
  );
});
