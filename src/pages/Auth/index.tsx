import { Routes, Route, Navigate, Link } from "react-router-dom";

import { useTranslation } from "react-i18next";

import { PageSignUp } from "./SignUp";
import { SignIn } from "./SignIn";
import { PagePasswordReset } from "./PasswordReset";
import { PagePasswordResetRequest } from "./PasswordResetRequest";
import NotVerified from "./NotVerified";
import Verify from "./Verify";
import { PageAccountConfirmation } from "./AccountConfirmation";

import cls from "./auth.module.scss";

import { observer } from "mobx-react-lite";
import { appState } from "state";
import { LangSwitch } from "components/LangSwitch";

interface AuthRoutesProps {
  enableSignUp: boolean;
  tacTo?: string;
}

export const AuthRoutes = observer((props: AuthRoutesProps) => {
  const { enableSignUp, tacTo = "/terms" } = props;
  const { t } = useTranslation();

  const user = appState.auth.user;

  if (user && user.verified) {
    return <Navigate to="/dashboard" />;
  }

  return (
    <div className={cls.pageWrapper}>
      <h1 className={cls.appName}>{t("appName")}</h1>
      <Routes>
        {enableSignUp && <Route path="/sign-up" element={<PageSignUp />} />}
        <Route path="/verify" element={<Verify />} />
        <Route path="/sign-in" element={<SignIn />} />
        <Route path="/password-reset/:resetToken" element={<PagePasswordReset />} />
        <Route path="/password-reset" element={<PagePasswordResetRequest />} />
        <Route
          path="/account-confirmation/:confirmationToken"
          element={<PageAccountConfirmation />}
        />
        <Route path="/not-verified" element={<NotVerified />} />
        <Route path="*" element={<Navigate to="/auth/sign-in" />} />
      </Routes>
      <p>
        <Link to={tacTo}>{t("linkTerms")}</Link>
      </p>
      <LangSwitch />
    </div>
  );
});
