import createSimpleComponent from "components/createSimpleComponent";
import Widget from "components/Widget";

import cls from "./simple-container.module.scss";

export default createSimpleComponent({
  displayName: "SimpleContainer",
  className: cls.wrapper,
  Component: Widget,
});
