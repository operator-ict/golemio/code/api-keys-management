import * as React from "react";
import { Link } from "react-router-dom";
import { useTranslation, Trans } from "react-i18next";
import { Formik } from "formik";
import * as validation from "validation";
import SimpleContainer from "../components/SimpleContainer";
import { useAuthStateContext } from "modules/auth";
import { WidgetContent } from "components/WidgetContent";
import { FormInputGroup } from "components/FormInputGroup";
import { FInput } from "components/FInput";
import { Button } from "components/Button";
import cls from "./buildings-password.module.scss";
import * as Yup from "yup";

export const PagePasswordResetRequest = () => {
    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const [hasRequested, setHasRequested] = React.useState(false);
    const [hasSubmitted, setSubmit] = React.useState(false);

    const { isLoading } = appState.auth.passwordResetRequestState;

    const handleClickReset = (data: { email: string }) => {
        appState.auth.passwordResetRequest({ email: data.email }).then((res: unknown) => {
            setHasRequested(!!res);
        });
    };

    return (
        <React.Fragment>
            <SimpleContainer>
                {hasRequested ? (
                    <WidgetContent style={{ textAlign: "center", padding: "4rem" }}>
                        <h2>{t("page::passwordRequest::titleSuccess")}</h2>
                        <p>{t("page::passwordRequest::asterixSuccess")}</p>
                    </WidgetContent>
                ) : (
                    <WidgetContent>
                        <h2>{t("page::passwordRequest::title")}</h2>
                        <br />

                        <Formik
                            initialValues={{ email: "", confirmation_password: "" }}
                            onSubmit={handleClickReset}
                            validateOnBlur={hasSubmitted}
                            validateOnChange={false}
                            validationSchema={Yup.object().shape({
                                email: validation.email,
                                confirmation_password: validation.hfield,
                            })}
                        >
                            {({ handleSubmit }) => (
                                <form
                                    onSubmit={(e) => {
                                        setSubmit(true);
                                        handleSubmit(e);
                                    }}
                                >
                                    <FormInputGroup>
                                        <FInput label={t("email")} name="email" placeholder={t("form::emailPlaceholder")} />
                                        <FInput className={cls.hfield} name="confirmation_password" />
                                        <Button isLoading={isLoading} primary type="submit">
                                            {t("page::passwordRequest::resetButton")}
                                        </Button>
                                    </FormInputGroup>
                                </form>
                            )}
                        </Formik>
                    </WidgetContent>
                )}
            </SimpleContainer>

            <p>
                <Trans i18nKey="auth::wantToLogIn">
                    {"Want to log in? "}
                    <Link to="/auth/sign-in">Go to login page.</Link>
                </Trans>
            </p>
        </React.Fragment>
    );
};
