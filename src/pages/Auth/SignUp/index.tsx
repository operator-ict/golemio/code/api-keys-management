import { Button } from "components/Button";
import { FInput } from "components/FInput";
import { FormInputGroup } from "components/FormInputGroup";
import Widget from "components/Widget";
import { WidgetContent } from "components/WidgetContent";
import { Formik } from "formik";
import { useAuthStateContext } from "modules/auth";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import * as validation from "validation";
import * as Yup from "yup";
import cls from "./buildings-signup.module.scss";

export const PageSignUp = () => {
    const [hasSubmitted, setSubmit] = React.useState(false);
    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const navigate = useNavigate();

    const signingUp = appState.auth.signUpState.isLoading;

    const handleSignUp = (data: { email: string; password: string; confirmation_password: string }) => {
        appState.auth.signUp({ email: data.email, password: data.password }).then((res: unknown) => {
            if (res) {
                navigate("/auth/verify");
            }
            return res;
        });
    };

    return (
        <React.Fragment>
            <Widget className={cls.widgetWrapper}>
                <div className={cls.content}>
                    <WidgetContent className={cls.left}>
                        <h2>{t("page::signup::title")}</h2>
                        <p>{t("page::signup::asterix")}</p>
                        <br />
                        <Formik
                            initialValues={{ confirmation_password: "", email: "", password: "" }}
                            onSubmit={handleSignUp}
                            validateOnBlur={hasSubmitted}
                            validateOnChange={false}
                            validationSchema={Yup.object().shape({
                                confirmation_password: validation.hfield,
                                email: validation.email,
                                password: validation.password,
                            })}
                        >
                            {({ handleSubmit }) => (
                                <form
                                    onSubmit={(e) => {
                                        setSubmit(true);
                                        handleSubmit(e);
                                    }}
                                >
                                    <FormInputGroup>
                                        <FInput label={t("email")} name="email" placeholder={t("form::emailPlaceholder")} />
                                        <FInput label={t("password")} name="password" type="password" />
                                        <FInput className={cls.hfield} name="confirmation_password" />
                                        <br />
                                        <Button className={cls.btnAction} isLoading={signingUp} success type="submit">
                                            {t("page::signup::signupButton")}
                                        </Button>
                                    </FormInputGroup>
                                </form>
                            )}
                        </Formik>
                    </WidgetContent>
                    <div className={cls.right}>
                        <h3 className={cls.headline}>{t("page::signup::appname")}</h3>
                        <p className={cls.desc}>{t("page::signup::appdesc")}</p>
                    </div>
                </div>
            </Widget>
            <p>
                <Trans i18nKey="auth::wantToLogIn">
                    {"Want to log in? "}
                    <Link to="/auth/sign-in">Go to login page.</Link>
                </Trans>
            </p>
        </React.Fragment>
    );
};
