import { default as CopyToClipboard } from "react-copy-to-clipboard";
import moment from "moment";
import { useTranslation } from "react-i18next";

import {
  Button,
  CircularProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  createStyles,
} from "@material-ui/core";

import * as io from "io-ts";
import { deleteApiKeyQueryStateObservableFactory } from "state/modules/apiKeys";
import { appState } from "state";
import { useState, useCallback, useMemo, FC } from "react";
import { KeyItem } from "pages/HomePage/components/KeyItem";
import { ApiKeyIO } from "codecs/ApiKeyIO";
import { NotificationType } from "modules/notifications";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";

interface ActiveKeyItemProps {
  item: io.TypeOf<typeof ApiKeyIO>;
}

const useStyles = makeStyles(() =>
  createStyles({
    flex: {
      display: "flex",
    },
    wrapper: {
      display: "flex",
      alignItems: "center",
    },
  })
);

export const ActiveKeyItem = observer(({ item }: ActiveKeyItemProps) => {
  const { t } = useTranslation();
  const deleteApiKeyQueryStateObservable = useMemo(
    () => deleteApiKeyQueryStateObservableFactory(),
    []
  );
  const [deleting, setDeleting] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const handleClose = useCallback(() => {
    setDeleteModalOpen(false);
  }, []);

  const classes = useStyles();

  return (
    <div className={classes.flex}>
      <div>
        <KeyItem
          title={
            <>
              {t("created_at")}:{" "}
              <span>{moment(item.created_at).format("LLLL")}</span>
            </>
          }
          content={item.code}
        />
      </div>
      <Box pl={2} className={classes.wrapper}>
        <CopyToClipboard
          onCopy={() => {
            appState.notifications.addNotification({
              title: "snacks::keyCopied",
              type: NotificationType.success,
            });
          }}
          text={item.code}
        >
          <Button size="small" color="primary">
            {t("copy")}
          </Button>
        </CopyToClipboard>

        <Button
          onClick={() => {
            setDeleteModalOpen(true);
          }}
          size="small"
          color="secondary"
        >
          {t("delete")}
        </Button>

        {deleteModalOpen && (
          <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={deleteModalOpen}
            onClose={handleClose}
          >
            <DialogTitle id="form-dialog-title">
              {t("deleteKeyModal::title")}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t("deleteKeyModal::asterix")}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                color="primary"
                onClick={async () => {
                  setDeleting(true);
                  try {
                    await deleteApiKeyQueryStateObservable.fetch({
                      params: { id: item.id },
                    });
                  } catch (error) {
                    appState.notifications.addNotification({
                      title: "snacks::keyDeleteError",
                      type: NotificationType.error,
                    });
                    setDeleting(false);
                    return;
                  }
                  if (deleteApiKeyQueryStateObservable.error) {
                    appState.notifications.addNotification({
                      title: "snacks::keyDeleteError",
                      type: NotificationType.error,
                    });
                    setDeleting(false);
                    return;
                  }
                  await appState.apiKeys.fetch({});
                  setDeleting(false);
                  handleClose();
                  appState.notifications.addNotification({
                    title: "snacks::keyDeleted",
                    type: NotificationType.success,
                  });
                }}
              >
                {deleting && (
                  <>
                    <CircularProgress size={20} />
                    &nbsp;&nbsp;
                  </>
                )}
                {t("deleteKeyModal::deleteButton")}
              </Button>
              <Button color="primary" onClick={handleClose}>
                {t("deleteKeyModal::cancelButton")}
              </Button>
            </DialogActions>
          </Dialog>
        )}
      </Box>
    </div>
  );
});
