import React, { ReactNode } from "react";
import { Box, createStyles, makeStyles, Typography } from "@material-ui/core";

interface KeyItemProps {
  title: ReactNode;
  content: ReactNode;
}

const useStyles = makeStyles(() =>
  createStyles({
    title: {
      textTransform: "uppercase",
    },
    keyContainer: {
      whiteSpace: "pre-wrap",
      wordBreak: "break-all",
    },
  })
);

export const KeyItem = ({ title, content }: KeyItemProps) => {
  const classes = useStyles();

  return (
    <Box py={2}>
      <Box className={classes.title}>
        <Typography component="div" color="textSecondary" variant="subtitle2">
          {title}
        </Typography>
      </Box>
      <div className={classes.keyContainer}>
        <code>{content}</code>
      </div>
    </Box>
  );
};
