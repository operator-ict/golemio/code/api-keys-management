import * as React from 'react';
import { useTranslation } from 'react-i18next';

export const TermsAndConditions = () => {
  const { t } = useTranslation();
  function createMarkup() {
    return { __html: t('terms_and_conditions') };
  }
  return <div dangerouslySetInnerHTML={createMarkup()} />;
};
