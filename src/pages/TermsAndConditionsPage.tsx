import React from 'react';
import { AppLayout } from 'components';
import { TermsAndConditions } from 'pages/components/TermsAndConditions';
import { Container, Box } from '@material-ui/core';
import { ROUTE_DASHBOARD } from 'routes';

export const TermsAndConditionsPage = () => {
  return (
    <AppLayout backButtonTo={ROUTE_DASHBOARD.create({})}>
      <Container maxWidth="lg">
        <Box pt={4}>
          <TermsAndConditions />
        </Box>
      </Container>
    </AppLayout>
  );
};
