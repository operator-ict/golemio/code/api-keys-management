import React from 'react';
import classnames from 'classnames';
import cls from './loader-dots.module.scss';

interface LoaderDotsProps {
  className?: string;
}

const LoaderDots = (props: LoaderDotsProps) => {
  const { className = null } = props;

  return (
    <div className={classnames(cls.dotloader, className, 'btn-loader')}>
      <div className={cls.dotloader1} />
      <div className={cls.dotloader2} />
      <div className={cls.dotloader3} />
    </div>
  );
};

export default LoaderDots;
