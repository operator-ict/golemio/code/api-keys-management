import { Snack } from "../Snack";
import { observer } from "mobx-react-lite";
import { useNotificationsState } from "components/NotificationsStateProvider";

export const SnackContainer = observer(() => {
  const notifications = useNotificationsState();
  const snacks = notifications.notifications;

  return (
    <>
      {snacks.map((data) => (
        <Snack key={data.id} data={data} />
      ))}
    </>
  );
});
