import { NotificationsState } from "modules/notifications";
import { useContext, createContext, FC } from "react";

const NotificationsContext = createContext<NotificationsState>(null);

export const NotificationsStateProvider: FC<{
  state: NotificationsState;
}> = ({ state, children }) => {
  return <NotificationsContext.Provider value={state}>{children}</NotificationsContext.Provider>;
};

export const useNotificationsState = () => {
  const state = useContext(NotificationsContext);
  if (!state) {
    throw new Error("Wrap your app in NotificationsStateProvider");
  }
  return state;
};
