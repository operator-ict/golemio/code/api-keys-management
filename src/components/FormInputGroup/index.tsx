import { ElementType, ReactNode } from "react";
import classnames from "classnames";
import cls from "./form-input-group.module.scss";

interface FormInputGroupProps {
  className?: string;
  children: ReactNode;
  Component?: ElementType;
  direction?: "vertical" | "horizontal";
  horizontal?: boolean;
  [prop: string]: unknown;
}

export const FormInputGroup = (props: FormInputGroupProps) => {
  const {
    children,
    className = null,
    Component = "div",
    direction = "vertical",
    horizontal = false,
    ...rest
  } = props;

  return (
    <Component
      className={classnames(className, {
        [cls.horizontal]: direction === "horizontal" || horizontal,
        [cls.vertical]: !(direction === "horizontal" || horizontal),
      })}
      {...rest}
    >
      {children}
    </Component>
  );
};
