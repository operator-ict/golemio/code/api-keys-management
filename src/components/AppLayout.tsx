import React, { FC } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  MenuItem,
  Menu,
  makeStyles,
  Box,
} from "@material-ui/core";
import { ToggleButton, ToggleButtonGroup } from "@material-ui/lab";
import { createStyles, Theme } from "@material-ui/core/styles";
import { Link, useNavigate } from "react-router-dom";
import ArrowBack from "@material-ui/icons/ArrowBack";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { useTranslation } from "react-i18next";
import { ROUTE_AUTH, ROUTE_DASHBOARD } from "routes";
import { appState } from "state";
import { useAuthStateContext } from "modules/auth";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    leftButton: {
      marginRight: theme.spacing(2),
    },
    header: {
      flexGrow: 1,
      display: "flex",
      alignItems: "center",
    },
    logo: {
      display: "block",
      height: 36,
    },
  })
);

export const AppLayout: FC<{ backButtonTo?: string }> = ({ children, backButtonTo }) => {
  const classes = useStyles({});
  const { t } = useTranslation();
  const { auth } = useAuthStateContext();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const navigate = useNavigate();

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          {!!backButtonTo && (
            <IconButton
              edge="start"
              color="inherit"
              component={Link}
              to={backButtonTo}
              className={classes.leftButton}
            >
              <ArrowBack />
            </IconButton>
          )}
          {/* <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton> */}
          <Typography variant="h6" color="inherit" className={classes.header}>
            <img
              src="https://golemio.cz/themes/golem/assets/brand/GolemioWhite.svg"
              alt=""
              className={classes.logo}
            />
            <Box pl={3}>{t("appName")}</Box>
          </Typography>

          <Box pr={2}>
            <ToggleButtonGroup
              size="small"
              value={appState.settings.lng}
              exclusive
              onChange={(event, value) => {
                if (value) {
                  appState.settings.setLng(value);
                }
              }}
            >
              <ToggleButton key={1} value="en" color="inherit">
                EN
              </ToggleButton>
              <ToggleButton key={2} value="cs">
                CS
              </ToggleButton>
            </ToggleButtonGroup>
          </Box>

          {!!auth.user ? (
            <>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                edge="end"
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem
                  onClick={(e) => {
                    auth.signOut();
                    handleClose();
                    navigate(ROUTE_DASHBOARD.create({}));
                  }}
                >
                  {t("sign_out")}
                </MenuItem>
              </Menu>
            </>
          ) : (
            <Button color="inherit" variant="outlined" component={Link} to={ROUTE_AUTH.create({})}>
              {t("signIn")}
            </Button>
          )}
        </Toolbar>
      </AppBar>
      {children}
    </>
  );
};
