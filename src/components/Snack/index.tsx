import { useCallback, FC } from "react";
import { useTranslation } from "react-i18next";

import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
// import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from "@material-ui/icons/Close";
// import WarningIcon from '@material-ui/icons/Warning';
import {
  IconButton,
  Snackbar,
  Slide,
  Box,
  SnackbarContent,
  createStyles,
  makeStyles,
} from "@material-ui/core";
import { green, red } from "@material-ui/core/colors";
import { useNotificationsState } from "components/NotificationsStateProvider";
import { Notification, NotificationType } from "modules/notifications";

const useStyles = makeStyles<{ notificationType: NotificationType }>(({ notificationType }) =>
  createStyles({
    notificationsWrapper: {
      backgroundColor: notificationType === NotificationType.error ? red[500] : green[500],
    },
    notification: {
      display: "flex",
      alignItems: "center",
    },
  })
);

const variantIcon = {
  success: CheckCircleIcon,
  //   warning: WarningIcon,
  error: ErrorIcon,
  //   info: InfoIcon
};

interface AlertBoxProps {
  data: Notification;
}

export const Snack = ({ data }: AlertBoxProps) => {
  const { t } = useTranslation();
  const notifications = useNotificationsState();

  const handleNotificationHoverStart = useCallback(
    (event) => {
      notifications.stopNotificationDecay(event.currentTarget.dataset.nid);
    },
    [notifications]
  );
  const handleNotificationHoverEnd = useCallback(
    (event) => {
      notifications.startNotificationDecay(event.currentTarget.dataset.nid);
    },
    [notifications]
  );
  const handleNotificationDismiss = useCallback(
    (event) => {
      notifications.removeNotification(event.currentTarget.dataset.nid);
    },
    [notifications]
  );
  const Icon = variantIcon[data.type];

  const classes = useStyles(data.type);

  return (
    <Snackbar
      open
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      TransitionComponent={Transition}
      data-nid={data.id}
      onMouseEnter={handleNotificationHoverStart}
      onMouseLeave={handleNotificationHoverEnd}
    >
      <SnackbarContent
        className={classes.notificationsWrapper}
        message={
          <div className={classes.notification}>
            {!!Icon && <Icon />}
            <Box pl={!!Icon ? 2 : 0}>
              {typeof data.title === "string" ? <p>{t(data.title)}</p> : data.title}
            </Box>
          </div>
        }
        action={[
          <IconButton
            key="close"
            aria-label="close"
            color="inherit"
            onClick={handleNotificationDismiss}
            data-nid={data.id}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      />
    </Snackbar>
  );
};

const Transition: FC = (props) => <Slide {...props} direction="left" />;
