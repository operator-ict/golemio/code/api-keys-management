import { useEffect } from "react";
import { useLocation } from "react-router-dom";

const normalizePath = (path) => path.replace(/\/+$/, "").toLowerCase();
const excludedPaths = ["/auth/account-confirmation"].map(normalizePath);

const isPathExcluded = (currentPath) => excludedPaths.some((excludedPath) => currentPath.includes(excludedPath));

const loadPlausibleScript = () => {
    const existingScript = document.querySelector("script[src='https://plausible.io/js/script.js']");
    if (existingScript) return;

    const script = document.createElement("script");
    script.src = "https://plausible.io/js/script.js";
    script.defer = true;
    script.setAttribute("data-domain", window.ENV?.DATA_DOMAIN);
    script.onerror = () => {
        console.error("Failed to load Plausible script. Check network or script URL.");
    };
    document.head.appendChild(script);
};

const AnalyticsTracker = () => {
    const location = useLocation();

    useEffect(() => {
        const currentPath = normalizePath(location.pathname);
        const isExcluded = isPathExcluded(currentPath);

        if (!isExcluded) {
            loadPlausibleScript();
        }
    }, [location.pathname]);

    return null;
};

export default AnalyticsTracker;
