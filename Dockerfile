ARG BASE_IMAGE=node:20.12.2-alpine

FROM $BASE_IMAGE AS build
WORKDIR /app

ARG PUBLIC_URL=/api-keys
COPY package.json yarn.lock tsconfig.json ./
COPY src src
COPY public public
RUN yarn --ignore-scripts && \
    yarn build

FROM $BASE_IMAGE
WORKDIR /app

COPY --from=build /app/build .
RUN yarn global add --ignore-scripts serve@^14.0.1

# Remove busybox links
RUN busybox --list-full | \
    grep -E "bin/ifconfig$|bin/ip$|bin/netstat$|bin/nc$|bin/poweroff$|bin/reboot$" | \
    sed 's/^/\//' | xargs rm -f

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

# Disable persistent history
RUN touch /app/.ash_history && \
    chmod a=-rwx /app/.ash_history && \
    chown root:root /app/.ash_history

USER nonroot

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]
