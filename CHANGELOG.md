# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.2.8] - 2024-11-25

### Added

-   add plausible analytics

## [0.2.7] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

## [0.2.6] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [0.2.5] - 2024-02-19

### Fixed

-   Fixed Honeypot ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [0.2.4] - 2024-02-07

### Removed

-   Remove Recaptcha ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [0.2.3] - 2024-01-21

### Changed

-   Translations - change Apiary links to Swagger ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))

## [0.2.2] - 2023-09-13

### Removed

-   Unnecessary info from JWT token ([permission-proxy#160](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/160))

## [0.2.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [0.2.0] - 2023-01-23

### Changed

-   Docker image optimization
